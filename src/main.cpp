/*
 * main.cpp
 *
 *  Created on: Oct 1, 2019
 *      Author: 4205_12
 */
#include<iostream>
#include<opencv2/opencv.hpp>
#include<time.h>
#include<string>
#include<sstream>
using namespace std;
using namespace cv;
/**
\mainpage
Le programme permet la capture d'une s'equence vid'eo de 5 secondes. L'utilisateur est libre de s'electionner la r'esolution parmis la liste de r'esolution supportees par le modele de cam'era
**/
int main()
{
    int number_of_resolution = 14;
	string resolution[number_of_resolution] = {"176 x 144", "160 x 120", "320 x 176", "320 x 176", "320 x 240", "352 x 288", "432 x 240", "800 x 600", "864 x 480", "960 x 544", "960 x 720", "1184 x 656", "1280 x 720", "1280 x 960"};
	int selected_resolution_index;
	int width, height;
	string selected_resolution;
	double fps;
	int nombre_images;
	cout << "Choose resolution in : \r";
	for (int i=0; i< number_of_resolution; i++){
		cout << i+1 << " - " << resolution[i] << "\r";
	}
	cin >> selected_resolution_index;
	selected_resolution_index =  selected_resolution_index - 1;
	selected_resolution = resolution[selected_resolution_index];
	std::istringstream iss (selected_resolution.substr(0, selected_resolution.find(" ")));
	iss >> width;
	std::istringstream iss2 (selected_resolution.substr(selected_resolution.find_last_of(" "), selected_resolution.size()-1));
	iss2 >> height;
	cout << "selected resolution : " << width << " x " << height << endl;
    VideoCapture capture(0);
    capture.set(CV_CAP_PROP_FRAME_WIDTH,width);
    capture.set(CV_CAP_PROP_FRAME_HEIGHT,height);
    if(!capture.isOpened()){
	    cout << "Failed to connect to the camera." << endl;
    }
    Mat frame, edges;

    int frames=2;
	for(int i=0; i<frames; i++){
		capture >> frame;
		if(frame.empty()){
		cout << "Failed to capture an image" << endl;
		return -1;
		}
		cvtColor(frame, edges, CV_BGR2GRAY);
		Canny(edges, edges, 0, 30, 3);
	}

    struct timespec start, end;
    clock_gettime( CLOCK_REALTIME, &start );

    frames=10;
    for(int i=0; i<frames; i++){
    	capture >> frame;
    	if(frame.empty()){
		cout << "Failed to capture an image" << endl;
		return -1;
    	}
    	cvtColor(frame, edges, CV_BGR2GRAY);
    	Canny(edges, edges, 0, 30, 3);
    }

    clock_gettime( CLOCK_REALTIME, &end );
    double difference = (end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec)/1000000000.0;
    cout << "It took " << difference << " seconds to process " << frames << " frames" << endl;
    fps = frames/difference;
    cout << "Capturing and processing " << frames/difference << " frames per second " << endl;

    nombre_images = (int)(fps * 5.0);

    cout << "nombre d'images : " << nombre_images << endl;


	#ifndef FIMAGE
	cv::VideoWriter video("capture-liv1.avi",CV_FOURCC('M', 'J', 'P', 'G') ,fps,cv::Size(width, height));
	#endif

	frames=nombre_images;
	for(int i=0; i<frames; i++){
		capture >> frame;
		if(frame.empty()){
		cout << "Failed to capture an image" << endl;
		return -1;
		}
		cvtColor(frame, edges, CV_BGR2GRAY);
		Canny(edges, edges, 0, 30, 3);
		video.write(frame);
	}

	#ifndef FIMAGE
	video.release();
	#endif
    return 0;
}
